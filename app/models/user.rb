# require 'active_record'

# puts "#{@users}"

class User < ActiveRecord::Base
  validates :username, presence: true, uniqueness: true
  validates :password, presence: true

  has_many :posts

  def all_user
    @users = User.all
    # result = conn.exec('SELECT * FROM <users> ORDER BY id DESC')

  end

end


# obj = User.new
# obj.all_user
