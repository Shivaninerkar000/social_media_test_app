class Post < ActiveRecord::Base
  validates :title, :content, presence: true

  belongs_to :users
  has_many :ratings

  def all_post
    # binding.pry
    @post.all
  end
end