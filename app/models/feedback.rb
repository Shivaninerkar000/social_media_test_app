class Feedback < ActiveRecord::Base
  validates :comment, presence: true
  
  belongs_to :users
  belongs_to :post

  def feedback_from_same_user
  end
end