require 'active_record'
require_relative './models/user'
require_relative './models/post'
require_relative './models/rating'
require_relative './models/feedback'

# Database connection
def db_configuration
  db_configuration_file = File.join(File.expand_path('..', __FILE__), '..', 'db', 'config.yml')
  YAML.load(File.read(db_configuration_file))
end
ActiveRecord::Base.establish_connection(db_configuration["development"])

# Create User
print "Give me the username of the User: "
username = gets.chomp
puts username

print "Give me the password of the user: "
password = gets.chomp
puts password

user = User.new(username: username, password: password)
user.save!
puts "Current User ID: #{user.id }"
puts "Current user: #{user.username}"
puts "Number of users in your database: #{User.count}"
# End user Model

# Create Post 
  print "Give me the title of the Post: "
	title = gets.chomp
	puts title

	print "Give me the content of the Post: "
	content = gets.chomp
	puts content

	@post = Post.new(title: title, content: content, user_id: user.id)
	@post.save!
	puts "ID: #{@post.id }"
	puts "title: #{@post.title}"
	puts "title: #{@post.content}"
	puts "user ID: #{user.id}"

	puts "Number of users in your database: #{User.count}"
# End post model


# Create Rating 
  print "Give the rating of the Post for #{@post.id} which belongs to #{user.id}: "
	rating = gets.chomp
	puts rating

	rating_of_post = Rating.new(rate: rating, post_id: @post.id, user_id: user.id)
	rating_of_post.save!

	puts "Rating is #{rating_of_post.rate} for #{@post.title} which belong to #{user.username}"

	puts "Number of rating in your database: #{Rating.count}"
# End rating model


# Create feedback 
  print "Give feedback for the User: "
	comment_for_user = gets.chomp
	puts comment_for_user

 #  print "Give feedback for the Post: "
	# comment_for_post = gets.chomp
	# puts comment_for_post

	# if comment_for_user.nil?	
	#   @feedback =Feedback.new(comment: comment_for_post,user_id: user.id, post_id: @post.id, owner_id: user.id)
	# 	@feedback.save!
	# else
	  @feedback = Feedback.new(comment: comment_for_user, user_id: user.id, post_id: @post.id, owner_id: user.id)
		@feedback.save!
	# end

	puts "ID: #{@feedback.id }"
	puts "title: #{@feedback.comment}"
	puts "Number of feedback in your database: #{Feedback.count}"
# End feedback model